$(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
        $('.carousel').carousel({
          interval: 1000
        });

        $('#primerBoton').removeClass('btn-outline-success');
        $('#primerBoton').addClass('btn-outline-warning');

        $('#primerBoton').on('show.bs.modal', function (e) {
          console.log("Boton número 1");          
        })
        $('#primerBoton').on('shown.bs.modal', function (e) {
          console.log('Boton número 1');
        })
        $('#primerBoton').on('hide.bs.modal', function (e) {
          console.log('Boton número 1');
        })
        $('#primerBoton').on('hidden.bs.modal', function (e) {
          console.log('Boton número 1');
        })
      })